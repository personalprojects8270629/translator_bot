package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

type Config struct {
	DbHost   string
	DbPort   string
	DbUser   string
	DbPass   string
	DbName   string
	ApiKey   string
	ApiUrl   string
	BotToken string
}

func Load() Config {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file: ", err)
	}
	return Config{
		DbHost:   os.Getenv("DB_HOST"),
		DbPort:   os.Getenv("DB_PORT"),
		DbUser:   os.Getenv("DB_USER"),
		DbPass:   os.Getenv("DB_PASS"),
		DbName:   os.Getenv("DB_NAME"),
		ApiKey:   os.Getenv("API_KEY"),
		ApiUrl:   os.Getenv("API_URL"),
		BotToken: os.Getenv("BOT_TOKEN"),
	}
}
