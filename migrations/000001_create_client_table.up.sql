CREATE TABLE IF NOT EXISTS clients(
    id SERIAL PRIMARY KEY,
    chat_id INT,
    first_name VARCHAR(250),
    last_name VARCHAR(250),
    step INT,
    lang VARCHAR(100),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);