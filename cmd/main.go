package main

import (
	"database/sql"
	"fmt"
	bot2 "github.com/translator_bot/bot"
	"github.com/translator_bot/config"
	"github.com/translator_bot/storage/postgres"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	cfg := config.Load()
	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.DbHost, cfg.DbPort, cfg.DbUser, cfg.DbPass, cfg.DbName,
	)
	conn, err := sql.Open("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err.Error())
	}
	strg := postgres.NewStorage(conn)

	bot := bot2.New(cfg, strg)

	bot.Run()
}
