package bot

const (
	startBotText = "<b>UZ: </b>Botdan foydalanish tilini tanlang\n\n<b>EN: </b>Select the language to use the bot"
)

var (
	menuText = map[string]string{
		"uz": "Tarjima qilish uchun <b>Tarjimon</b> tugmasini bosing",
		"en": "Click the <b>Translator</b> button to translate",
	}
)