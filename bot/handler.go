package bot

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	k "github.com/translator_bot/bot/keyboards"
	"github.com/translator_bot/pkg/models"
)

func (h *BotHandler) StartBot(update tgbotapi.Update) {
	err := h.storage.GetORCreate(update.Message.Chat.ID, &models.Client{
		ChatID:    update.Message.Chat.ID,
		FirstName: update.Message.From.FirstName,
		LastName:  update.Message.From.LastName,
		Step:      0,
	})
	if err != nil {
		log.Println("Error GetORCreate(): ", err.Error())
	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, startBotText)
	msg.ReplyMarkup = k.LanguageKeybaord
	msg.ParseMode = "HTML"
	if _, err := h.bot.Send(msg); err != nil {
		log.Fatal("Error sent start message: ", err)
	}
}

func (h *BotHandler) HandleLang(text string, update tgbotapi.Update) {
	lang := languageChange(text)
	err := h.storage.ChangeLang(update.Message.Chat.ID, lang)
	if err != nil {
		log.Println("error change lang: ", err)
	}
	user, err := h.storage.Get(update.Message.Chat.ID)
	if err != nil {
		log.Printf("Error: %v", err)
	}
	if text == Lang[user.Lang] {
		msg := tgbotapi.NewMessage(user.ChatID, menuText[user.Lang])
		msg.ReplyMarkup = k.TranslatorKeyboard[user.Lang]
		msg.ParseMode = "HTML"
		if _, err := h.bot.Send(msg); err != nil {
			log.Fatal("Error send menu message: ", err)
		}
		err = h.storage.ChangeStep(msg.ChatID, 1)
		if err != nil {
			log.Println("Error change step: ", err.Error())
		}
	}
}

func languageChange(lang string) string {
	if lang == "🇺🇿 O'zbekcha" {
		return "uz"
	} else if lang == "🇺🇸 English" {
		return "en"
	}
	return "uz"
}
