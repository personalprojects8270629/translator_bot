package keyboards

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

var LanguageKeybaord = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("🇺🇿 O'zbekcha"),
		tgbotapi.NewKeyboardButton("🇺🇸 English"),
	),
)

var TranslatorKeyboard = map[string]tgbotapi.ReplyKeyboardMarkup{
	"uz": tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("Tarjimon"),
		),
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("⬅️ Ortga"),
		),
	),
	"en": tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("Translator"),
		),
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("⬅️ Back"),
		),
	),
}
