package bot

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/translator_bot/config"
	"github.com/translator_bot/storage/repo"
)

type BotHandler struct {
	cfg     config.Config
	storage repo.StorageI
	bot     *tgbotapi.BotAPI
}

func New(cfg config.Config, strg repo.StorageI) BotHandler {
	bot, err := tgbotapi.NewBotAPI(cfg.BotToken)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = true

	return BotHandler{
		cfg:     cfg,
		storage: strg,
		bot:     bot,
	}
}

func (h *BotHandler) Run() {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := h.bot.GetUpdatesChan(u)
	for update := range updates {
		go h.HandleBot(update)
	}
}

func (h *BotHandler) HandleBot(msg tgbotapi.Update) {
	step, err := h.storage.GetStep(msg.Message.Chat.ID)
	if err != nil {
		log.Println("Error get step: ", err.Error())
	}
	if msg.Message.Command() == "start" {
		h.StartBot(msg)
	} else if msg.Message.Text != "" {
		switch step {
		case 0:
			h.HandleLang(msg.Message.Text, msg)
		}
	}
}
