-include .env

DB_URL="postgresql://$(DB_USER):$(DB_PASS)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable"

run:
	go run cmd/main.go

file:
	migrate create -ext sql -dir migrations -seq create_client_table

migrate_up:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrate_up1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migrate_down:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migrate_down1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1

.PHONY: start migrateup migratedown