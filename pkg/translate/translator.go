package translate

import (
	"fmt"
	"io"
	"net/http"
	"strings"
)

func Translator(source string, sourceLang, toLang string) (string, error) {
	url := "https://google-translate1.p.rapidapi.com/language/translate/v2/detect"

	payload := strings.NewReader("q=" + source + "&target=" + toLang + "&source=" + sourceLang)

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept-Encoding", "application/gzip")
	req.Header.Add("X-RapidAPI-Key", "43987b919fmshf4a72d9eba56355p1ea6adjsn24526c538817")
	req.Header.Add("X-RapidAPI-Host", "google-translate1.p.rapidapi.com")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	//fmt.Println(res)
	fmt.Println(string(body))
	return string(body), nil
}
