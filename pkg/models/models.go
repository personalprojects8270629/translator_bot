package models

type Client struct {
	Id        int
	ChatID    int64
	FirstName string
	LastName  string
	CreatedAt string
	Lang      string
	Step      int
}
