package repo

import "github.com/translator_bot/pkg/models"

type StorageI interface {
	Create(client *models.Client) error
	Get(chatID int64) (*models.Client, error)
	ChangeStep(chatID int64, step int) error
	GetStep(chatID int64) (int, error)
	ChangeLang(chatID int64, lang string) error
	GetORCreate(chatID int64, client *models.Client) error
}
