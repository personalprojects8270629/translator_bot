package postgres

import (
	"database/sql"
	"errors"
	"github.com/translator_bot/pkg/models"
	"github.com/translator_bot/storage/repo"
)

type StoragePg struct {
	db *sql.DB
}

func NewStorage(db *sql.DB) repo.StorageI {
	return StoragePg{
		db: db,
	}
}

func (r StoragePg) Create(req *models.Client) error {
	_, err := r.db.Exec(`
INSERT INTO 
    clients(
            chat_id, 
            first_name, 
            last_name, step
            )
VALUES (
        $1, $2, $3, $4
        )`,
		req.ChatID,
		req.FirstName,
		req.LastName,
		req.Step)
	if err != nil {
		return err
	}
	return nil
}

func (r StoragePg) Get(chatId int64) (*models.Client, error) {
	var res models.Client
	err := r.db.QueryRow(`
SELECT 
    chat_id, 
    first_name, 
    last_name, step, lang 
FROM 
    clients WHERE chat_id=$1`, chatId).
		Scan(
			&res.ChatID,
			&res.FirstName,
			&res.LastName,
			&res.Step, &res.Lang,
		)
	if err != nil {
		return nil, err
	}
	return &res, err
}

func (r StoragePg) ChangeStep(chatId int64, step int) error {
	_, err := r.db.Exec(`UPDATE clients SET step=$1 WHERE chat_id=$2`, step, chatId)
	if err != nil {
		return err
	}
	return nil
}

func (r StoragePg) GetStep(chatId int64) (int, error) {
	var step int
	err := r.db.QueryRow(`SELECT step FROM clients WHERE chat_id=$1`, chatId).Scan(step)
	if err != nil {
		return 0, err
	}
	return step, err
}

func (r StoragePg) ChangeLang(chatId int64, lang string) error {
	_, err := r.db.Exec(`UPDATE clients SET lang=$1 WHERE chat_id=$2`, lang, chatId)
	if err != nil {
		return err
	}
	return nil
}

func (r StoragePg) GetORCreate(chatID int64, client *models.Client) error {
	_, err := r.Get(chatID)
	if errors.Is(err, sql.ErrNoRows) {
		err := r.Create(&models.Client{
			ChatID:    chatID,
			FirstName: client.FirstName,
			LastName:  client.LastName,
			Step:      client.Step,
		})
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}
	return nil
}
